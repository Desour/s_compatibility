-- SPDX-FileCopyrightText: 2023 DS
--
-- SPDX-License-Identifier: CC0-1.0

if not minetest.register_on_receiving_chat_message then
	minetest.register_on_receiving_chat_message = minetest.register_on_receiving_chat_messages
else
	minetest.register_on_receiving_chat_messages = minetest.register_on_receiving_chat_message
end
